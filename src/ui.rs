use bevy::prelude::*;

use crate::{NR_SKILLS, SKILL_BOX_SIDE};

#[derive(Component)]
pub struct CooldownClockContainer(pub usize);

#[derive(Component)]
pub struct CooldownClock {
    pub start_cooldown: f32,
    pub ability: usize,
}
impl CooldownClock {
    pub fn new(ability: usize, start_cooldown: f32) -> Self {
        Self {
            ability,
            start_cooldown,
        }
    }
}

#[derive(Component)]
pub struct ContainsClock;

#[derive(Bundle)]
pub struct ClockBundle {
    clock: CooldownClock,
    node: NodeBundle,
}
impl ClockBundle {
    pub fn new(ability: usize, start_cooldown: f32) -> Self {
        Self {
            clock: CooldownClock::new(ability, start_cooldown),
            node: NodeBundle {
                style: Style {
                    width: Val::Px(SKILL_BOX_SIDE),
                    height: Val::Percent(100.0),
                    ..default()
                },
                background_color: Color::rgb(0.4, 0.4, 0.8).into(),
                ..default()
            },
        }
    }
}

pub fn ui_setup(mut commands: Commands) {
    // UI
    commands
        .spawn(NodeBundle {
            style: Style {
                width: Val::Px(NR_SKILLS as f32 * SKILL_BOX_SIDE),
                height: Val::Px(SKILL_BOX_SIDE),
                align_self: AlignSelf::End,
                ..default()
            },
            border_color: Color::rgb(0.65, 0.65, 0.65).into(),
            ..default()
        })
        // Cooldown background
        .with_children(|parent| {
            for i in 0..NR_SKILLS {
                parent.spawn((
                    NodeBundle {
                        style: Style {
                            min_width: Val::Px(SKILL_BOX_SIDE),
                            height: Val::Px(SKILL_BOX_SIDE),
                            align_items: AlignItems::End,
                            ..default()
                        },
                        background_color: Color::rgb(0.65, 0.65, 0.65).into(),
                        ..default()
                    },
                    CooldownClockContainer(i),
                ));
            }
        });
}

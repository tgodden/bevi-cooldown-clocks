use bevy::prelude::*;

mod ui;

const DEFAULT_CD_SEC: f32 = 1.;
pub const SKILL_BOX_SIDE: f32 = 400.;
pub const NR_SKILLS: usize = 2;

#[derive(Component)]
struct Player;

#[derive(Component)]
struct Ability(usize);
#[derive(Component)]
struct CooldownTimer(Timer);
impl Default for CooldownTimer {
    fn default() -> Self {
        Self(Timer::from_seconds(DEFAULT_CD_SEC, TimerMode::Once))
    }
}
#[derive(Bundle)]
struct AbilityCooldown {
    ability: Ability,
    timer: CooldownTimer,
}
impl AbilityCooldown {
    fn new(ability: usize) -> Self {
        Self {
            ability: Ability(ability),
            timer: Default::default(),
        }
    }
}

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup, (setup, ui::ui_setup))
        .add_systems(
            Update,
            (
                handle_keyboard,
                update_timers,
                add_cooldown_ui,
                update_cooldown_ui,
            ),
        )
        .run();
}

fn setup(mut commands: Commands) {
    // Camera
    commands.spawn(Camera3dBundle::default());
    // Player
    commands.spawn(Player).with_children(|parent| {
        for i in 0..2 {
            parent.spawn(Ability(i));
        }
    });
}

fn handle_keyboard(
    mut commands: Commands,
    keyboard: Res<ButtonInput<KeyCode>>,
    mut query: Query<(Entity, &Ability), Without<CooldownTimer>>,
) {
    for (entity, ability) in &mut query {
        if keyboard.pressed(KeyCode::KeyQ) && ability.0 == 0 {
            commands
                .entity(entity)
                .insert(AbilityCooldown::new(ability.0));
        }
        if keyboard.pressed(KeyCode::KeyW) && ability.0 == 1 {
            commands
                .entity(entity)
                .insert(AbilityCooldown::new(ability.0));
        }
    }
}

fn update_timers(
    mut commands: Commands,
    mut cooldowns_q: Query<(Entity, &mut CooldownTimer)>,
    time: Res<Time>,
) {
    for (entity, mut timer) in &mut cooldowns_q {
        if timer.0.tick(time.delta()).finished() {
            info!("{:?}: Cooldown {:?}", entity, timer.0.elapsed());
            commands.entity(entity).remove::<CooldownTimer>();
        }
    }
}

fn add_cooldown_ui(
    mut commands: Commands,
    cooldown_timers: Query<&Ability, With<CooldownTimer>>,
    clock_containers: Query<(Entity, &ui::CooldownClockContainer), Without<ui::ContainsClock>>,
) {
    for ability in &cooldown_timers {
        if let Some((entity, container)) = clock_containers.iter().find(|(_e, c)| c.0 == ability.0)
        {
            let clock = commands.spawn(ui::ClockBundle::new(container.0, 1.0)).id();
            info!("New overlay {:?} for ability {}", clock, ability.0);
            let mut entity = commands.entity(entity);
            entity.add_child(clock);
            entity.insert(ui::ContainsClock);
        }
    }
}

fn update_cooldown_ui(
    mut commands: Commands,
    without_cooldown_timers: Query<&Ability, Without<CooldownTimer>>,
    clock_containers: Query<(Entity, &ui::CooldownClockContainer), With<ui::ContainsClock>>,
    with_cooldown_timers: Query<(&Ability, &CooldownTimer)>,
    mut clocks: Query<(Entity, &ui::CooldownClock, &mut Style)>,
) {
    for ability in &without_cooldown_timers {
        if let Some((entity, _container)) = clock_containers.iter().find(|(_e, c)| c.0 == ability.0)
        {
            info!("Removing from {:?}", entity);
            let mut entity = commands.entity(entity);
            entity.despawn_descendants();
            entity.remove::<ui::ContainsClock>();
        }
    }
    for (ability, timer) in &with_cooldown_timers {
        if let Some((entity, clock, mut style)) =
            clocks.iter_mut().find(|(_e, c, _s)| c.ability == ability.0)
        {
            //info!("Changing height of {:?}", entity);
            let percentage = 1.0 - timer.0.elapsed().as_secs_f32() / clock.start_cooldown;
            style.height = Val::Percent(percentage * 100.0);
        }
    }
}
